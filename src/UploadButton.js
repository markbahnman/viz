import React from 'react';

function UploadButton({ onChange }) {
  return (
    <form className="upload" onSubmit={e => e.preventDefault()}>
      <label htmlFor="file-upload" className="file-upload">
        {'Import Data'}
      </label>
      <input
        type="file"
        name="file"
        id="file-upload"
        onChange={e => onChange(e.target.files[0], 'test')}
      />
      <button className="upload-button" type="submit">
        Upload
      </button>
    </form>
  );
}

export default UploadButton;
