import React, { useState } from 'react';

function SearchBar({ text, onChange }) {
  const [input, setInput] = useState('');
  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        onChange(input);
      }}
    >
      {' '}
      <input
        value={input}
        onChange={e => setInput(e.target.value)}
        placeholder="search"
        type="text"
        name="searchTerm"
      />{' '}
      <button
        type="submit"
        onSubmit={e => {
          e.preventDefault();
          onChange(e.target.value);
        }}
      >
        Search
      </button>{' '}
    </form>
  );
}

export default SearchBar;
