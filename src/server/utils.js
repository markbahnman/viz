// Quick utility functino to truncate numbers to a give decimal
function toFixed(num, fixed) {
  var re = new RegExp('^-?\\d+(?:.\\d{0,' + (fixed || -1) + '})?');
  return num.toString().match(re)[0];
}

async function sendToES(client, index, data) {
  // TODO add better error handling, retry support
  try {
    await client.bulk({
      refresh: true,
      body: data.flatMap(doc => [{ index: { _index: index } }, doc])
    });
  } catch (err) {
    console.log('Error sending documents to ES', err);
  }
}

// Get the timestamp of the first day of the month which is used as a key
// for elasticsearch aggregation buckets when bucketed monthly
function getMonthlyKey(date) {
  const dateObj = new Date(date);

  return `${dateObj.getFullYear()}-${('0' + (dateObj.getMonth() + 1)).slice(
    -2
  )}-01T00:00:00.000Z`;
}

// TODO this function mutates the argument d, refactor this or better yet move this entire calculation to the database
function calculateWieghtedRankings(monthly_totals, d) {
  const monthly_total = monthly_totals[getMonthlyKey(d.date)];

  for (const engine of [
    'google_rank',
    'google_base_rank',
    'bing_rank',
    'yahoo_rank'
  ]) {
    if (d[engine] && d[engine] !== '') {
      // TODO figure out why ES is returning strings even though they are typed as integers
      const weight =
        Number(d[engine]) *
        Number(d.global_monthly_searches) /
        Number(monthly_total);
      d[`${engine}_weighted`] = weight ? toFixed(weight, 4) : '';
    } else {
      d[`${engine}_weighted`] = '';
    }
  }

  return d;
}

function generateQueryWithAggregations(term, start, end) {
  const dateFilters = [];

  if (start) {
    dateFilters.push({ gte: start });
  }

  if (end) {
    dateFilters.push({ lte: end });
  }

  // TODO Big hardcoded query with aggregations, break this down
  const query = {
    aggs: {
      google_total: {
        filter: { exists: { field: 'google_rank' } },
        aggs: {
          total_searches: { sum: { field: 'global_monthly_searches' } }
        }
      },
      yahoo_total: {
        filter: { exists: { field: 'yahoo_rank' } },
        aggs: {
          total_searches: { sum: { field: 'global_monthly_searches' } }
        }
      },
      bing_total: {
        filter: { exists: { field: 'bing_rank' } },
        aggs: {
          total_searches: { sum: { field: 'global_monthly_searches' } }
        }
      },
      google_base_rank_total: {
        filter: { exists: { field: 'google_base_rank' } },
        aggs: {
          total_searches: { sum: { field: 'global_monthly_searches' } }
        }
      },
      search_by_month: {
        date_histogram: {
          field: 'date',
          calendar_interval: 'month'
        },
        aggs: {
          monthly_totals: { sum: { field: 'global_monthly_searches' } }
        }
      }
    },
    _source: [
      'date',
      'site',
      'keyword',
      'google_rank',
      'google_base_rank',
      'yahoo_rank',
      'bing_rank',
      'global_monthly_searches',
      'regional_monthly_searches'
    ],
    query: { bool: { must: [], should: [], filter: [] } }
  };

  if (dateFilters.length > 0) {
    query.query.bool.filter.push({
      range: {
        date: dateFilters.reduce((acc, d) => {
          return { ...acc, ...d };
        }, {})
      }
    });
  }

  if ((!term || term === '') && dateFilters.length === 0) {
    query.query = { match_all: {} };
  } else if (!term || term !== '') {
    query.query.bool.filter.push({
      bool: {
        should: {
          match: {
            keyword: term
          }
        }
      }
    });
  }

  return query;
}

module.exports = {
  sendToES,
  calculateWieghtedRankings,
  generateQueryWithAggregations
};
