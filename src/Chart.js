import React from 'react';
import { RadialChart } from 'react-vis';

function Chart({ data }) {
  if (data && data.length > 0) {
    return (
      <div className="chart">
        <RadialChart
          showLabels={data.length > 0}
          data={data}
          width={300}
          height={300}
        />
      </div>
    );
  }

  return <div className="chart" />;
}

export default Chart;
