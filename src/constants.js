export const TABLE_HEADERS = [
  {
    Header: 'Data',
    columns: [
      {
        Header: 'Date',
        accessor: 'date'
      },
      {
        Header: 'Site',
        accessor: 'site'
      },
      {
        Header: 'Keyword',
        accessor: 'keyword'
      }
    ]
  },
  {
    Header: 'Search Rankings',
    columns: [
      {
        Header: 'Google',
        accessor: 'google_rank',
        sortType: 'basic'
      },
      {
        Header: 'Weighted',
        accessor: 'google_rank_weighted',
        sortType: 'basic'
      },
      {
        Header: 'Google Base Rank',
        accessor: 'google_base_rank',
        sortType: 'basic'
      },
      {
        Header: 'Weighted',
        accessor: 'google_base_rank_weighted',
        sortType: 'basic'
      },
      {
        Header: 'Yahoo',
        accessor: 'yahoo_rank'
      },
      {
        Header: 'Weighted',
        accessor: 'yahoo_rank_weighted',
        sortType: 'basic'
      },
      {
        Header: 'Bing Rank',
        accessor: 'bing_rank',
        sortType: 'basic'
      },
      {
        Header: 'Weighted',
        accessor: 'bing_rank_weighted',
        sortType: 'basic'
      }
    ]
  },
  {
    Header: 'Traffic Data',
    columns: [
      {
        Header: 'Global Monthly Searches',
        accessor: 'global_monthly_searches',
        sortType: 'basic'
      },
      {
        Header: 'Regional Monthly Searches',
        accessor: 'regional_monthly_searches',
        sortType: 'basic'
      }
    ]
  }
];
