function uploadFile(file, user) {
  let formData = new FormData();
  formData.append('file', file);

  fetch(`/api/upload/${user}`, {
    method: 'POST',
    body: formData
  })
    .then(success => {
      console.log('SUCCESS', success.status);
      return success;
    })
    .catch(error => console.log(error));
}

const onSubmit = async (file, user) => {
  // TODO add better error handling
  try {
    await uploadFile(file, user);
  } catch (error) {
    console.log('Error uploading file', error);
    return;
  }

  // TODO this is really hacky. We should only return and refresh when the data has been process and indexed
  setTimeout(() => window.location.reload(), 3000);
};

export default { onSubmit };
