require('array.prototype.flatmap').shim();
const { Client } = require('@elastic/elasticsearch');
const express = require('express');
const multer = require('multer');
const csv = require('fast-csv');
const fs = require('fs');
const {
  calculateWieghtedRankings,
  sendToES,
  generateQueryWithAggregations
} = require('./server/utils.js');

const app = express();
const port = process.env.PORT || 3001;
const client = new Client({ node: 'http://localhost:9200' });
const upload = multer({ dest: 'uploads/' });

// Responds with csv file that user can download
app.get('/api/export', async (req, res) => {
  // TODO this ended up being a lot of the same code as the search api
  // refactor to reduce duplicated code
  let result;

  const { term, start, end } = req.query;

  try {
    result = await client.search({
      index: 'test',
      size: process.env.DEFAULT_SIZE || 10000,
      body: generateQueryWithAggregations(term, start, end)
    });
  } catch (e) {
    console.log('ERROR', JSON.stringify(e, null, 2));

    res.sendStatus(500);
    return;
  }

  const data = result.body.hits.hits.map(res => res._source);
  const sum_by_month = result.body.aggregations.search_by_month.buckets.reduce(
    (acc, o) => {
      acc[o.key_as_string] = o.monthly_totals.value;
      return acc;
    },
    {}
  );

  // Add weighted ranking
  const data_with_weighted_ranking = data.map(d =>
    calculateWieghtedRankings(sum_by_month, d)
  );

  const headers = Object.keys(data_with_weighted_ranking[0]);
  const rows = data_with_weighted_ranking.map(o => Object.values(o));

  const buffer = await csv.writeToBuffer([headers, ...rows]);

  res.status(200).send(buffer);
});

app.get('/api/search', async (req, res) => {
  let result;
  const { term, start, end } = req.query;

  try {
    result = await client.search({
      index: 'test',
      size: process.env.DEFAULT_SIZE || 500,
      body: generateQueryWithAggregations(term, start, end)
    });
  } catch (e) {
    console.log('ERROR', JSON.stringify(e, null, 2));

    res.sendStatus(500);
    return;
  }

  const data = result.body.hits.hits.map(res => res._source);
  const sum_by_month = result.body.aggregations.search_by_month.buckets.reduce(
    (acc, o) => {
      acc[o.key_as_string] = o.monthly_totals.value;
      return acc;
    },
    {}
  );

  // Add weighted ranking
  const data_with_weighted_ranking = data.map(d =>
    calculateWieghtedRankings(sum_by_month, d)
  );

  res.send({
    data: data_with_weighted_ranking,
    aggs: result.body.aggregations
  });
});

// Validates input data from user and imports it into elasticsearch
app.post('/api/upload/:user', upload.single('file'), (req, res, next) => {
  /// TODO Add user validation
  const { user } = req.params;
  const file = req.file;
  let buffer = [];

  if (!file) {
    console.log('No file given');
    return res.sendStatus(400);
  } else if (file && file.mimetype !== 'text/csv') {
    // TODO create a more generic parsing interface to enable more input data types
    console.log('Wrong file format');
    return res.send(400, 'Required file format is text/csv');
  }

  console.log(`Received request for ${user}`, file);

  fs
    .createReadStream(file.path)
    .pipe(
      csv.parse({
        renameHeaders: true,
        headers: [
          'date',
          'site',
          'keyword',
          'market',
          'location',
          'device',
          'google_rank',
          'google_base_rank',
          'yahoo_rank',
          'bing_rank',
          'google_url',
          'yahoo_url',
          'bing_url',
          'advertiser_competition',
          'global_monthly_searches',
          'regional_monthly_searches',
          'cpc',
          'tags'
        ]
      })
    )
    .on('data', async row => {
      if (buffer.length < 1000) {
        buffer.push(row);
      } else {
        const clonedBuffer = buffer.slice(0);
        buffer = [];
        // send to ES
        await sendToES(client, user, clonedBuffer);
      }
    })
    .on('error', e => {
      // TODO check to see if the stream stops on error, or just creates an event
      // if it doesn't stop, add handling of stream liftime
      console.log('ERROR parsing csv', e);
      next(e);
    })
    .on('end', async () => {
      // send to ES
      await sendToES(client, user, buffer);
      res.sendStatus(200);
    });
});

app.listen(port, () => console.log(`App listening on port ${port}!`));
