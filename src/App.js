import 'react-dates/initialize';
import React, { useState, useMemo, useEffect } from 'react';
import { DateRangePicker } from 'react-dates';
import FileSaver from 'file-saver';

import './App.css';
import 'react-dates/lib/css/_datepicker.css';
import { TABLE_HEADERS } from './constants';
import Chart from './Chart.js';
import Table from './Table.js';
import SearchBar from './SearchBar.js';
import UploadButton from './UploadButton.js';
import FileHandlers from './fileHandlers.js';

function apiQueryUrl(slug, term, date) {
  return `/api/${slug}?term=${term}&start=${
    date.start ? date.start.toISOString() : ''
  }&end=${date.end ? date.end.toISOString() : ''}`;
}

function App() {
  const columns = useMemo(() => TABLE_HEADERS, []);

  // TODO this is a lot of state for a very simple vizualization and bound to get out of hand
  // test how the a reducer might simplify things
  const [term, setTerm] = useState('');
  const [date, setDate] = useState({ start: null, end: null });
  const [data, setData] = useState([]);
  const [focus, setFocus] = useState(null);
  const [chartData, setChartData] = useState([]);

  useEffect(
    () => {
      const fetchData = async () => {
        // TODO we're fetching the data and aggregations on every call, might be unnecessary
        const response = await fetch(apiQueryUrl('search', term, date));
        const result = await response.json();
        setData(result.data);

        let aggData = [];

        if (result.data.length > 0) {
          for (const key of [
            'google_total',
            'google_base_rank_total',
            'bing_total',
            'yahoo_total'
          ]) {
            aggData.push({
              angle: result.aggs[key].total_searches.value,
              label: key
            });
          }
        }

        setChartData(aggData);
      };

      fetchData();
    },
    [term, date]
  );

  return (
    <div className="App">
      <header className="App-header">
        <UploadButton onChange={FileHandlers.onSubmit} />
        <b className="title">Untitled Data Visualization</b>
        <div
          className="export-button"
          onClick={async () => {
            const data = await fetch(apiQueryUrl('export', term, date));
            const blob = await data.blob();

            FileSaver.saveAs(blob, 'export.csv');
          }}
        >
          Export
        </div>
      </header>
      <Chart data={chartData} />
      <div className="filters">
        <DateRangePicker
          isOutsideRange={() => false}
          startDate={date.start}
          startDateId="query_start_date"
          endDate={date.end}
          endDateId="query_end_date"
          onDatesChange={({ startDate, endDate }) =>
            setDate({ start: startDate, end: endDate })
          }
          focusedInput={focus}
          onFocusChange={focusedInput => setFocus(focusedInput)}
        />
        <SearchBar text={term} onChange={setTerm} />
      </div>
      <Table columns={columns} data={data} />
    </div>
  );
}

export default App;
