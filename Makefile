clean:
	@curl -XDELETE http://localhost:9200/test

setup:
	@curl -XPUT http://localhost:9200/test  \
		-H "Content-Type: application/json" \
		--data-binary "@es_mapping.json"

reset: clean setup

seed:
	@docker-compose up logstash
