# Viz

A simple data visualization app

# Getting setup

This project uses elasticsearch as its primary data store which can be quickly setup using docker and the docker-compose file, so you'll need [Docker](https://www.docker.com/products/docker-desktop) and [Docker Compose](https://docs.docker.com/compose/install/) setup first. It also makes use of a Makefile so having make installed will be very helpful

## Setting up the database

```
docker-compose up -d elasticsearch
```

Will download the container and get it running in the background. To seed the database we'll use a csv file of data and logstash to quickly get it indexed.

The appropriate index and mapping can be done with `make setup` which will setup the `/test` index. The mapping file can be found in `es_mapping.json`

To play around with you can use logstash to import the data into your database, or use the import function in the app. If you want to use logstash, edit the `pipeline/001.conf` file to read the daata file and run logstash with `docker-compose up -d logstash`

## Setting up the app

To install the dependencies, run

```
npm install
```

which will install the dependencies for both the api server and the frontend app.

## Running the app

In a separate terminal window run the api server with

```
node src/server.js
```

and by default the server will run on port 3001

Then start the react app with

```
npm start
```

which will spin up the dev server, at which point you can go to localhost:3000 in your browser to interact with the app.

# Using the app

To start with, unless you previously imported data into elasticsearch you'll have to import data. For now the only accepted file format is csv. Click the `Import Data` button on the top left of the page and select the file, such as the `proper.csv` file in this repo.

## Querying data

You can query the data by date range and keyword. By default it will return all the data available in the index, but you can select the date range with the date range picker, or search by keyword. The relative searches for each provider will show up in a radial chart for the data retrieved.

## Exporting data

You can export the filtered data with the `Export` button in the top right of the page.

## Things not complete

### Users

jhere isn't really a concept of users as is in the app, it's basicaly a single user app. If I were to implement it I would give each use an index (behind an alias for easier reindexing) and probably use a three table setup in a relational database for the actual user data (AccountTable <-> AccountUserRelationshipTable <-> UserTable). This is assuming that the customers using this app include those either working for a company (The company has an account and gives user accounts to its employees) or an agency (Single User that has roles in many accounts).

### Data features

There is a limit as to the amount of data that is sent to the frontend, and it is simply overwritten when we get new data so there isn't any caching or prefecting. There also isn't a way to paginate through the data.

# Next steps

1) Tear it all down and start from scratch!
  - With only a couple hours of development time, you're really just testing out hypotheses' and golden paths, not thinking about maintainability and how the code will change over time. Never be tempted to keep MVPs just because the "work is already done!". Good enough 'for now' is more often than not 'forever'
2) Typescript!
  - Raw(ish) node is great for prototyping and quickly putting something together, but once we want to acutally move forward with something we want something a bit more robust than the pure dynamicism of javascript (unless you like having a macro to write checking for undefined|null for every paramter of every function).
3) Fully spin off api
  - It could even be in a new language! It doesn't have to live in the same repo as the frontend, but it could (monorepos are a thing) and if they're both in typescript they could share interfaces!
4) Replace heavy-weight third-party components
  - Prototyping or even an MVP with components such as react-dates is fine, but it is often (as is in this case) simulatenously too featureful and too constraining
5) Dockerize for consistent deployments
